## TTL - Transistor-transistor Logic
Transistor–transistor logic (TTL) is a logic family built from bipolar junction transistors. Its name signifies that transistors perform both the logic function (the first "transistor") and the amplifying function (the second "transistor"), as opposed to resistor–transistor logic (RTL) or diode–transistor logic (DTL).

TTL integrated circuits (ICs) were widely used in applications such as computers, industrial controls, test equipment and instrumentation, consumer electronics, and synthesizers. Sometimes TTL-compatible logic levels are not associated directly with TTL integrated circuits, for example, they may be used at the inputs and outputs of electronic instruments.



# SIMPLE TTL TO RS232 CONVERTER BOARD

This unit is simply a RS232 to TTL converter IC on a board with
connectors. We created this unit as a quick means to allow any TTL level
uart to connect to the standard RS232 levels of a serial port. This unit can
be powered from 3.0V to 5.5V and meets ElA/TIA-232 and V.28/V.24
specifications, even at VCC = 3.0V.
Its features are:
1 transmit and 1 receive line, full duplex. RTS and CTS lines are
also incorporated on board via solder pad area.
Small unit size of 48 x 36mm
On board DB9 female connector for direct connect to a serial port.
4 mounting holes
![INT232-A](http://priority1design.com.au/rs232a.jpg)

# PORT POWERED TTL TO RS232 CONVERTER

This unit is similar to our INT232-A above but has additional circuitry
that draws power directly from the receive line of the RS232 port.
This avoids the need to provide a separate power supply to the unit.
The RS232 side has a DB9 female connector, while the TTL side has
a DB9 male connector.

# features are:
1 transmit and 1 receive line, full duplex.
Port powered from the RS232 Receive line (pin 3).
DB9 connectors packaged in a slimline case.
![INT232-B](http://priority1design.com.au/rs232b.jpg)
                                          
# SIMPLE TTL TO RS485 CONVERTER BOARD

The TTL485-NHM board provides a half-duplex multi-drop / multi-point serial link
for a network between 2 and 32 Nodes. On board links are provided for enabling
line termination (120 ohms) as well as bus fail-safe mode. Provision is made for
connection of cable shield with current limiting (120 ohms).

# features are:
Single supply 5.0V +/- 10% @ 5mA.
Up to 250kbps data rate..
Maximum distance between end-nodes is 1200m.
Up to 32 nodes per network.
Small board size: 48mm x 36mm x 15mm.
+/- 15KV ESD protection.
Half-duplex operation.
RS422 backward compatible.
Suitable for multi-drop network.
Designed for multi-point network.
Available with either Crimp or IDC connectors.
![TTL485-NHM-C](http://priority1design.com.au/rs485_converter_1.jpg)
![TTL485-NHM-I](http://priority1design.com.au/rs485_converter_2.jpg)

# Raspberry Pi for Serial Pi

RS232 to TTL adapter should find at least four connections, some circuits do come with more connections, but the only four you need is: VCC (IC Power-supply pin), TX (Transmitted Data), RX (Received Data) and GND (Ground power-supply pin)

It can connect the wires directly to the GPIO Pins or use the breadboard as a middleman as we did in this tutorial. We mainly did this as we didn’t have any female to female breadboard wire available to us.

Wiring your RS232 to TTL adapter to your Raspberry Pi is a simple process, with it requiring only 4 of the GPIO connecting to be wired to the serial connector, even better all 4 GPIO pins needed are in a row, so it is easy to follow. Make use of our table and guide below to connect your serial connector to your Raspberry Pi.

VCC connects to Pin 4.
TX connects to Pin 8.
RX connects to Pin 10.
GND connects to Pin 6.
![circuit](https://pi.lbbcdn.com/wp-content/uploads/2017/08/serial-read-and-write-fitzrig.jpg)

# Overview of Modbus RS-232 and RS-485:

Modbus Remote Monitoring and Control traditionally uses RS-232 and RS-485 Modbus communication protocols for collecting data from Modbus slaves. Of the two, Modbus using the RS-485 protocol is more common than RS-232 due to its support for multi-drop communication.

These transports are not in any way limited to Modbus, of course. They're found in a wide variety of other applications. Modbus is just one possible protocol that can be sent via dedicated serial connection.

Modbus is a protocol that (traditionally) uses serial communication lines. Modbus over serial connects the master to Modbus slave devices for collecting register and coil information.

# Capabilities of Modbus RS-232 vs RS-485
# Modbus RS-232 Allows Concurrent, Full-Duplex Flow of Data:

Modbus via RS-232 sends data in the form of a time-series of bits. It is a standard for communication between data terminal and data circuit termination equipment. Full duplex transmission (Tx) and receive (Rx) for data occurs on different circuits when using Modbus RS-232 lines. That means that data is able to flow both ways at the same time.

# Modbus RS 485 Is Half-Duplex and Indicates Values Using Differences in Voltage:

Modbus RTU RS485 interface is similar, but different, from RS-232. This two-wire, multi-point connection communicates serial data by indicating values by sending different voltages across the two wires. These differences between these voltages are related to one and zero values, which make up the Modbus 485 communications.
