# IIOT Gateway

Hello Learners,  
First of all, thank you for showing your interest in this project. Myself Chinmay Bohra (@cbohra00627) and I am the maintainer of this project.  
**Active Contributers:**  
* Anusha M (@anushamaruprolu91)
* Tulasi Vinukonda (@tulasivinukonda)
* Apporva Gupta (@apoorva_gupta98)
* Lavanya T (@lavanyaramya1947)
  
## Introduction  
IOT Gateways provide remote access to industrial devices. These gateways are positioned between edge systems and the cloud. Devices like PLCs, Sensors, actuators etc. can be connected to the gateways. They perform functions such as protocol translation, data processing/storage/filtering, device security etc.  
Importance of an IOT Gateway can be understood through the following points:  
* It facilitates compatibility across the IOT network.
* It manages the connectivity of various industrial devices.
* It enables various kinds of devices to communicate with each other by managing their communication protocols.
* It can bring data analysis from remote network to the edge. This is called edge computing.
  
## IIOT Gateway Project  
In this project, we are building an industrial IOT Gateway. It is based on a raspberry pi and will be using [ShunyaOS APIs](http://demo.shunyaos.org/docs/diy/01-quickstart) to communicate with the PLCs and cloud services. This device will get data from PLCs and send the data to online dashboards for remote monitoring. To know all the features and the project process, please visit this [Project Planning Sheet](https://docs.google.com/spreadsheets/d/1uNRdVE0HA0IkdNZtOc6mkUz_9pVpdR3JcHePigGtfWU/edit?usp=sharing) and these [Tasks & Subtasks](https://gitlab.com/iotiotdotin/project-internship/iiot-gateway/-/issues). Here is the link to an example IIOT Gateway which is already in the market: [Advantech Uno-420](https://www.advantech.com/products/9a0cc561-8fc2-4e22-969c-9df90a3952b5/uno-420/mod_2d6a546b-39e3-4bc4-bbf4-ac89e6b7667c).
  
## Call to Contributers  
Now, to complete this project, we need skilled contributers. If you are interested in learning new things and contributing to this project, then please go through this [issue](https://gitlab.com/iotiotdotin/project-internship/iiot-gateway/-/issues/40) and comment the task which you want to contribute to by tagging me (@cbohra00627) in your comment.  
  
**If I don't have any skill yet I wish to contribute?**  
Don't worry, we have a skilling series with various modules which will make you familiar with the skills relevant to this project. Just go through this [skilling](https://gitlab.com/iotiotdotin/project-internship/orientation) and you will be able to learn many things.
  
## How to Contribute?  
If you wish to contribute to any tasks, then follow the following steps:  
1. Go to this [General issue](https://gitlab.com/iotiotdotin/project-internship/iiot-gateway/-/issues/40) and comment the tasks which you want to contribute to by choosing them from these [Tasks and Subtsasks](https://gitlab.com/iotiotdotin/project-internship/iiot-gateway/-/issues) and also write why do you think you can complete this task. Please, remember to tag me (@cbohra00627) in your comment.
2. You will be given some instructions in each selected issue, read them and work accordingly.
3. Then fork this whole repo and start commiting your work to your forked repo.
4. When you think your work is complete, then send me a merge request. After reviewing your work, it will be merged to the master branch.
  
You can always ask your doubts, any kind of problems or suggestions regarding the project process, project features etc. in the corresponding issues by tagging me (@cbohra00627) in your comment.  
  
## Final Words!  
If you choose to contribute to this project, you get some responsibility for the tasks you have choosen. Remember, you will learn only by doing things yourself. So, if you think you are struck at anywhere, please try to solve the problems first by yourself, give it some time (NOT MUCH!). And if even after giving some time and searching on google, you are not able solve it, then you can always ask me.  
It is upto you to make the most out of this opportunity.  
  
Thanks! Keep Learning!