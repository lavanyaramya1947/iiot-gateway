/** @file alert.cpp
    @brief send alert message to user's whatsapp or sms

    @ author Lavanya T
    */

/*
 *#####################################################################
 *  Initialization block
 *  ---------------------
 *  This block contains initialization code for this particular file.
 *#####################################################################
 */

 /*-----Standard Includes-----*/

#include <iostream> // input and output library
#include <stdlib.h> //file includes functions involving memory allocation, process control, conversions and others
#include <string.h> //to manipulate strings and arrays


/*-----Project Includes-----*/
#include <shunyaInterfaces.h> // to program embedded boards to talk to PLC's, Sensors, Actuators and Cloud 
 //header files to parse a json file
#include "include/rapidjson/document.h"
#include "include/rapidjson/writer.h"
#include "include/rapidjson/stringbuffer.h"
#include "include/rapidjson/filewritestream.h"
#include "include/rapidjson/prettywriter.h"
#include "include/rapidjson/filereadstream.h" 

/* using standard namespace */
using namespace std; 
/* using rapidjson namespace */
using namespace rapidjson;

/* Declare a JSON document. JSON document parsed will be stored in this variable */
Document document; 

/*declare an object to store the twilio object */
Value& twilio_object;

/* parse the json file */
void parse_file(void)
{
	
	/* Open the config.json file in read mode */
        FILE* fp = fopen("config.json", "r"); 

        /* Declare read buffer */
        char readBuffer[65536];

        /* Declare stream for reading the config stream */
        FileReadStream is(fp, readBuffer, sizeof(readBuffer));


        /* Parse config.json and store it in `document` */
        document.ParseStream(is);

        /* Close the config.json file*/
        fclose(fp);
	
}



int current_reading()
{
    //code to get current_reading of device from json file
    return current_reading;
}

/* function to initialize the twilio object */
void twilio_init()
{   
    // "twilio" contains SSID and auth token
    twilio_object=document["twilio"];
}

/* function to send message through sms */
void send_sms(float temp)
{    
    // declare a objects to store the mobile numbers 
    Value& from_num = document["twilio_num"]; 
    Value& to_num =   document["user_num"];

    //create a new twilio instance ,here twilio_object is name of the json object
    twilioObj sms = newTwilio(twilio_object); 
    
    //to send sms to user
    sendSmsTwilio(sms,from_num.GetString(),to_num.GetString(),"temperature is above %d deg celsius",temp); 
}

void send_whatssapp(float temp)
{     
    // declare a objects to store the mobile numbers 
    Value& from_num = document["twilio_num"]; 
    Value& to_num =   document["user_num"];

    //create a new twilio instance ,here twilio_object is name of the json object
    twilioObj whatsapp = newTwilio(twilio_object); 

    //to send message to user
    sendWhatsappTwilio(whatsapp,from_num.GetString(),to_num.GetString(),"temperature is above %d deg celsius",temp); 
   
}

int main(void)
{
     //get data from json file to know in which platform user want message
    Value& option = document["platform"];
    //get data from json file to know no of PLC's connected
    Value& no_of_plc = document["PLC"]["Num"];
    //convert number of PLC's string to integer
    number = (uint32_t)atoi(no_of_plc.GetString());


    shunyaInterfacesSetup(); // function to initiate shunya interfaces

    parse_file(); //to parse the json file

    //iterate the function over no of PLC's connected
    for(Value::ConstMemberIterator itr = PLC.MemberBegin(); itr != PLC.MemberEnd(); ++itr)
    {  
       //get current value of plc device 
      current_reading=current_value(); 
      //get threshold value of PLC
      Value& threshold_value=document["thres"];
      thres_value=(uint32_t)atoi(threshold_value.GetString())
      //code to send message if temperature reaches threshold
      if (current_reading >=thres_value)
        {
           if (option == "sms")
             {
                send_sms(thres_value); //call the function to send sms
            }
            else
            {
               send_whatssapp(thres_value); //call the function to send whatsapp msg
            }
        }

    }
   
   

   
    return 0;
    
   

}
